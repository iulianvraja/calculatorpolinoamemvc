package View;

import javax.swing.*;
import java.awt.*;

public class Interfata {
    private JFrame j;
    TextField t1;
    TextField t2;
    TextField t3;
    JButton b1;
    JButton b2;
    JButton b3;
    JButton b4;
    JButton b5;

    public Interfata() {
        j=new JFrame();
        j.setVisible(true);
        j.setBounds(40, 40, 600, 600);

        initialize();

    }

    private void initialize() {
        t1=new TextField(10);

      t2=new TextField(10);
        t3=new TextField(10);
        t1.setEditable(true);
        t1.setSize(100,40);
        t2.setSize(100,40);
        t3.setSize(100,40);

        JLabel l1=new JLabel("Polinom1");
        JLabel l2=new JLabel("Polinom2");
        JLabel l3=new JLabel("Rezultat");

        l1.setSize(40,40);
        l2.setSize(40,40);
        l3.setSize(40,40);

        JPanel f=new JPanel();
         b1=new JButton("Adunare");
        b2=new JButton("Scadere");
        b3=new JButton("Inmultire");
         b4=new JButton("Derivare");
        b5=new JButton("Integrare");

        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER));
        JPanel panel3 = new JPanel();

//panel1.set[Preferred/Maximum/Minimum]Size()
        panel1.add(b1);
        panel1.add(b2);
        panel1.add(b3);
        panel1.add(b4);
        panel1.add(b5);

        panel2.add(t1);
        panel2.add(t2);
        panel2.add(t3);

        panel3.add(l1);
        panel3.add(l2);
        panel3.add(l3);

        container.add(panel1);
        container.add(panel2);
        container.add(panel3);
        j.setContentPane(container);
        j.setVisible( true);
    }


public JButton getbadunare(){
        return b1;
}

    public JButton getbscadere(){
        return b2;
    }

    public JButton getinmultire(){
        return b3;
    }

    public JButton getbderivare(){
        return b4;
    }

    public JButton getbintegrare(){
        return b5;
    }
    public String getP1(){
        return t1.getText();
    }
    public String getP2(){
        return t2.getText();
    }
    public void setrez(String s){
        t3.setText(s);
    }
}
