package Model;

public class Adunare {
    protected Polinom x1;
    protected Polinom x2;
    public Adunare(Polinom s1, Polinom s2){
        x1=s1;
        x2=s2;
    }

    public String AdunaPolinoame(){
        String s="";
        int k;

        for(Monom i:x1.getPoli()) {
            boolean f=true;//presupunem ca nu va avea un momom cu acelasi coeficient
            for (Monom j : x2.getPoli()) {
                if (i.getPutere() == j.getPutere()) {
                    System.out.println("coef="+i.getCoeficient() +"\n"+"coef="+j.getCoeficient());
                    k = i.getCoeficient() + j.getCoeficient();
                    f=false;
                    s+=k+"X^"+i.getPutere()+"+";
                }

            }

            if(f==true)
                s+=i.getCoeficient()+"X^"+i.getPutere()+"+";
        }
        for(Monom i:x2.getPoli()) {
            boolean f=true;//presupunem ca nu va avea un momom cu acelasi coeficient
            for (Monom j : x1.getPoli()) {
                if (i.getPutere() == j.getPutere()) {
                    f=false;
                }
            }

            if(f==true)
                s+="+"+i.getCoeficient()+"X^"+i.getPutere();
        }

        return s;
    }

}
