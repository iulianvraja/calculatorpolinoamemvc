package Model;

import java.util.ArrayList;

public class Polinom {
   private ArrayList<Monom> poli;
    public Polinom(String s){
   poli=new ArrayList<Monom>();
       ImparteinMonoame(s);
    }

    public void ImparteinMonoame(String s){
        String [] Monomame=s.split("\\+");
        System.out.println("in poli monp[0]:"+Monomame[0]);
        for(String i:Monomame){
            System.out.println("i="+i);
            poli.add(new Monom(i));
        }
    }

    public void inverseazapoli(){
        for(Monom i:poli)
            i.setCoeficient(0-i.getCoeficient());
    }

    public ArrayList<Monom> getPoli() {
        return poli;
    }

    public void setPoli(ArrayList<Monom> poli) {
        this.poli = poli;
    }
}
