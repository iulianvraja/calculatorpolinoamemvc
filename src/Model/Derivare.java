package Model;

public class Derivare {


    Polinom x1;

    public Derivare(Polinom x){
        x1=x;
    }

    public String Derivare(){
        String s="";
        int p,p1;
        for(Monom i: x1.getPoli()){
            p=i.getPutere()*i.getCoeficient();
            p1=i.getPutere()-1;
            s+=p+"X^"+p1+"+";
        }

        return s;
    }
}
