package Model;

public class Scadere extends Adunare {
    public Scadere(Polinom p1, Polinom p2){
        super(p1,p2);
    }

    public String ScadePolinoame(){
        x2.inverseazapoli();
        String s=AdunaPolinoame();
        return s;
    }
}
