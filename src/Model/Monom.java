package Model;

public class Monom {
    private int coeficient;

    private int semn;
    private int putere;

    public Monom(String s){
      this.coeficient=spargestring(s)[0];
      this.putere=spargestring(s)[1];

    }

    public int[] spargestring(String s){
        int []v=new int[2];
        int semn=0;
        v[0]=1;
        if(s.charAt(0)=='-')
            semn=1;//trebuie sa formam un coeficient negativ
        if(s.charAt(0)<='9' && s.charAt(0)>='0')
                v[0]=Character.getNumericValue(s.charAt(0));
        else
            if(semn==1 && s.charAt(1)<='9' && s.charAt(1)>='0')
                v[0]=0-Character.getNumericValue(s.charAt(1));
            else
                if(semn==1)
                    v[0]=-1;
                else
                    v[0]=1;

        String[] arrOfStr = s.split("\\^");

        int size=arrOfStr.length;
        System.out.println("val="+arrOfStr[size-1]);
        v[1]=Integer.valueOf(arrOfStr[size-1]);
        return v;
    }

    public int getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(int coeficient) {
        this.coeficient = coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }
}
