package Model;

public class Integrare {
    Polinom x1;

    public Integrare(Polinom x){
        x1=x;
    }

    public String Integrare(){
        String s="";
        int p;
        for(Monom i: x1.getPoli()){
            p=i.getPutere()+1;
            s+=i.getCoeficient()+"/"+p+"X^"+p+"+";
        }

        return s;
    }
}
