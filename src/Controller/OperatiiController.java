package Controller;

import Model.*;
import View.Interfata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperatiiController {


    private static  Interfata view;

   public OperatiiController(Interfata v){
      view=v;
      actiunipebutoane();
   }

    private void actiunipebutoane() {
        //op adunare
        view.getbadunare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s1,s2;
                s1=view.getP1();
                s2=view.getP2();
                Polinom p1=new Polinom(s1);
                Polinom p2=new Polinom(s2);
                Adunare x=new Adunare(p1,p2);
                view.setrez(x.AdunaPolinoame());
            }
        });

        view.getbscadere().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s1,s2;
                s1=view.getP1();
                s2=view.getP2();
                Polinom p1=new Polinom(s1);
                Polinom p2=new Polinom(s2);
                Scadere x=new Scadere(p1,p2);
                view.setrez(x.ScadePolinoame());
            }
        });

        view.getinmultire().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s1,s2;
                s1=view.getP1();
                s2=view.getP2();
                Polinom p1=new Polinom(s1);
                Polinom p2=new Polinom(s2);
                Inmultire x=new Inmultire(p1,p2);
                view.setrez(x.InmultirePolinoame());

            }
        });

        view.getbderivare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s1,s2;
                s1=view.getP1();
                s2=view.getP2();
                Polinom p1=new Polinom(s1);
                Polinom p2=new Polinom(s2);
                Derivare x=new Derivare(p1);
                view.setrez(x.Derivare());
            }
        });


        view.getbintegrare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s1,s2;
                s1=view.getP1();
                s2=view.getP2();
                Polinom p1=new Polinom(s1);
                Polinom p2=new Polinom(s2);
                Integrare x=new Integrare(p1);
                view.setrez(x.Integrare());
            }
        });
    }


}
